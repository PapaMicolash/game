package servlets;

import com.google.gson.Gson;
import model.WrapperBattlefield;
import model.animals.Bear;
import model.animals.StingerInsect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "GetInsectsServlet" , urlPatterns = "/getInsects")
public class GetInsectsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WrapperBattlefield wb = WrapperBattlefield.getInstance();
        wb.startGame();
        ArrayList<StingerInsect> stingerInsects = wb.getBattlefield().getStingerInsects();
        Gson gson = new Gson();
        String stingerInsectsJson = gson.toJson(stingerInsects);
        System.out.println(stingerInsectsJson);
        if (stingerInsectsJson != null) {
            System.out.println("StingerInsect isn't null!!!");
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(stingerInsectsJson);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
