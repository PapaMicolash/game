package servlets;

import com.google.gson.Gson;
import model.WrapperBattlefield;
import model.animals.Bear;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GetAnimalServlet", urlPatterns = "/getAnimal")
public class GetAnimalServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WrapperBattlefield wb = WrapperBattlefield.getInstance();
        Bear bear = wb.getBattlefield().getBear();

        Gson gson = new Gson();
        String bearJson = gson.toJson(bear);
        System.out.println("bearJson is here!!!");

        if (bearJson != null) {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(bearJson);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
