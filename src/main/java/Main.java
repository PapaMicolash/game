import model.WrapperBattlefield;

public class Main {

    public static void main(String[] args) {
        WrapperBattlefield wb = WrapperBattlefield.getInstance();
        wb.startGame();
        System.out.println(wb.getBattlefield());
        System.out.println("==================================");
        wb.startTurn();
        System.out.println(wb.getBattlefield());
        System.out.println("==================================");
        wb.getBattlefield().clearBG();
        System.out.println(wb.getBattlefield());

    }
}
