package model.animals;

public abstract class StingerInsect extends Animal {
    private int timeAggression;

    public StingerInsect(Animal.State state, int hp) {
        super(state, hp);
    }

    public StingerInsect() {
    }

    public int getTimeAggression() {
        return timeAggression;
    }

    public void setTimeAggression(int timeAggression) {
        this.timeAggression = timeAggression;
    }

    abstract void sting(Animal animal);

    abstract int generateAggressionTime();

    void updateAggression() {
        timeAggression--;
        if (timeAggression < 1) {
            setState(State.RUNNING_AWAY);
        }
    }

    @Override
    public String toString() {
        return "StingerInsect{" +
                "state=" + getState() +
                ", hp=" + getHp() +
                "timeAggression=" + timeAggression +
                '}';
    }
}
