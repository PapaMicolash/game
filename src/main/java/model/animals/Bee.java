package model.animals;

public class Bee extends StingerInsect {
    public final String name = "Bee";
    public Bee() {
        super(State.AGGRESSIVE, 1);
        this.setTimeAggression(generateAggressionTime());
    }

    void sting(Animal animal) {
        animal.getHarm(20);
        setState(State.DEAD);
    }

    public void act(Animal animal) {

        if (animal.getState() == State.AGGRESSIVE) {
            int rand = (Math.random() < 0.7)?0:1; //check chance
            if (rand == 1) {
                sting(animal);
            } else {
                updateAggression();
            }
        }

    }

    int generateAggressionTime() {
        int num = 1 + (int)(Math.random() * 3);
        return num;
    }


    @Override
    public String toString() {
        return '\n' + "Bee{" +
                "state=" + getState() +
                ", hp=" + getHp() +
                ", timeAggression=" + getTimeAggression() +
                '}';
    }
}
