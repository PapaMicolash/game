package model.animals;

public class Bear extends Animal {

    public Bear() {
        super(State.AGGRESSIVE);
        setHp(generateHP());
    }

    public void act(Animal animal) {
        int rand = 0;
        if (animal.getState() == State.AGGRESSIVE) {
            if (animal instanceof StingerInsect){
                rand = (Math.random() < 0.5)?0:1;
                if (rand == 1) {
                    animal.getHarm(100);
                }
            }
        }
    }

    private int generateHP() {
        int num = 200 + (int)(Math.random() * 400);
        return num;
    }

    @Override
    public String toString() {
        return '\n' +  "Bear{" +
                "state=" + getState() +
                ", hp=" + getHp() +
                '}';
    }
}
