package model.animals;

public class Wasp extends StingerInsect {
    public final String name = "Wasp";
    public Wasp() {
        super(State.AGGRESSIVE, 1);
        this.setTimeAggression(generateAggressionTime());
    }

    void sting(Animal animal) {
        animal.getHarm(15);
        updateAggression();
    }

    public void act(Animal animal) {
        if (animal.getState() == State.AGGRESSIVE) {
            int rand = (Math.random() < 0.6)?0:1;
            if (rand == 1) {
                sting(animal);
            } else {
                rand = (Math.random() < 0.5)?0:1;
                if (rand == 1) {
                    updateAggression();
                }
            }
        }
    }

    int generateAggressionTime() {
        int num = 2 + (int)(Math.random() * 4);
        return num;
    }

    @Override
    public String toString() {
        return '\n' + "Wasp{" +
                "state=" + getState() +
                ", hp=" + getHp() +
                ", timeAggression=" + getTimeAggression() +
                '}';
    }
}
