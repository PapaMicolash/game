package model.animals;

public abstract class Animal {
    private State state;
    private int hp;

    public Animal(State state, int hp) {
        this.state = state;
        this.hp = hp;
    }

    public Animal(State state) {
        this.state = state;
    }

    public Animal() {
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getHp() {
        return hp;
    }

    void setHp(int hp) {
        this.hp = hp;
    }

    public void getHarm(int damage) {
        int hpAfterDamage = getHp() - damage;
        if (hpAfterDamage <= 0) {
            setState(State.DEAD);
        } else {
            setHp(hpAfterDamage);
        }
    }

    public abstract void act(Animal animal);

    public enum State {
        AGGRESSIVE, RUNNING_AWAY, DEAD;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "state=" + state +
                ", hp=" + hp +
                '}';
    }
}
