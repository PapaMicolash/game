package model;

import model.animals.*;

import java.util.ArrayList;
import java.util.Iterator;

public class Battlefield {
    Bear bear = new Bear();
    ArrayList<StingerInsect> stingerInsects = new ArrayList<StingerInsect>();

    public Bear getBear() {
        return bear;
    }

    public void setBear(Bear bear) {
        this.bear = bear;
    }

    public ArrayList<StingerInsect> getStingerInsects() {
        return stingerInsects;
    }

    public void setStingerInsects(ArrayList<StingerInsect> stingerInsects) {
        this.stingerInsects = stingerInsects;
    }

    public void generateStingerInsects() {
        int numInsects = 10 + (int)(Math.random() * 10);
        int type = 0;
        for (int i = 0; i < numInsects; i ++) {
            type = (Math.random() < 0.5)?0:1;
            if (type == 0) {
                stingerInsects.add(new Wasp());
            } else {
                stingerInsects.add(new Bee());
            }
        }
    }

    public void beginBattle() {
        int sISize = stingerInsects.size();
        if (sISize > 0) {
            int numInsect = 0 + (int)(Math.random() * sISize);

            bear.act(stingerInsects.get(numInsect));
            for (int i = 0; i < sISize; i++) {
                stingerInsects.get(i).act(bear);
            }
        }
    }

    public void clearBG() {
        Iterator<StingerInsect> stingerInsectIterator = stingerInsects.iterator();
        while(stingerInsectIterator.hasNext()) {
            StingerInsect stingerInsect = stingerInsectIterator.next();
            if (stingerInsect.getState() != Animal.State.AGGRESSIVE) {
                stingerInsectIterator.remove();
            }
        }
    }

    @Override
    public String toString() {
        return "Battlefield{" +
                "bear=" + bear + '\n' +
                "StingerInsects=" + stingerInsects +
                '}';
    }
}
