package model;

public class WrapperBattlefield {
    private static WrapperBattlefield ourInstance = new WrapperBattlefield();

    public static WrapperBattlefield getInstance() {
        return ourInstance;
    }

    private WrapperBattlefield() {
    }

    Battlefield battlefield;

    public Battlefield getBattlefield() {
        return battlefield;
    }

    public void setBattlefield(Battlefield battlefield) {
        this.battlefield = battlefield;
    }

    public void startGame() {
        battlefield = new Battlefield();
        battlefield.generateStingerInsects();
    }


    public void startTurn() {
        battlefield.beginBattle();
    }
}
